<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masyarakat extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "login"){
			redirect(base_url("auth/login"));
		}
		
	}


	public function index()
	{
		$this->load->view('komponen/komponen-website/kom-header');
		$this->load->view('komponen/komponen-website/kom-navbar');
		$this->load->view('website/v-index');
		$this->load->view('komponen/komponen-website/kom-footer');
	}

	public function buat_pengaduan()
	{
		$this->load->view('komponen/komponen-website/kom-header');
		$this->load->view('komponen/komponen-website/kom-navbar');
		$this->load->view('website/v-buat-pengaduan');
		$this->load->view('komponen/komponen-website/kom-footer');
	}

	public function data_pengaduan(){
		$this->load->view('komponen/komponen-website/kom-header');
		$this->load->view('komponen/komponen-website/kom-navbar');
		$this->load->view('website/v-data-pengaduan');
		
	}

	public function tanggapan_pengaduan($id)
	{
		$this->load->view('komponen/komponen-website/kom-header');
		$this->load->view('komponen/komponen-website/kom-navbar');
		$this->load->view('website/v-tanggapan-pengaduan');
		$this->load->view('komponen/komponen-website/kom-footer');
	}



}
