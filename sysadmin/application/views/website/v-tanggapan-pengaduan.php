<section class="contact-section section_padding">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="contact-title text-center mb-5">Tanggapan Pengaduan</h2>
      </div>
      <div class="col-md-12">
        <form class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
          <div class="row">
            <div class="col-12">
              <div class="form-group">
                  <label>Pengaduan Anda </label>
                  <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" readonly="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>
              </div>
            </div>
            <div class="col-12">
              <div class="form-group">
                  <label>Jawaban Admin</label>
                  <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" readonly="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>
              </div>
            </div>
          </div>
          <div class="form-group mt-3">
            <a href="<?php echo base_url('home/buat_pengaduan') ?>" class="button button-contactForm btn_1 col-md-12 text-center" style="border-radius: 30px">Buat Pengaduan Baru</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>