<?php 
 
class ModelUniv extends CI_Model{

	public function create($value, $table)
	{
		$this->db->insert($table, $value);
	}

	public function read($table)
	{
		return $this->db->get($table)->result();
	}

	public function update($where, $value, $table)
	{
		$this->db->where($where);
		$this->db->update($table,$value);
	}

	public function delete($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function cek_login($table,$where)
	{		
		return $this->db->get_where($table,$where);
	}

	public function select_data($table, $where)
	{
		return $this->db->get_where($table,$where)->result_array();
	}

	public function hitung_data($table)
	{
		$query = $this->db->get($table);
		return $query->num_rows();
	}

	public function hitung_data_where($where, $table)
	{		
		return $this->db->get_where($table, $where)->num_rows();
	}

	public function data_terakhir($limit)
	{
		$this->db->select('*');
		$this->db->from('pendaftaran');
		$this->db->order_by('id_pendaftaran', 'DESC');
		$this->db->limit($limit);
		$query = $this->db->get();
		return $query->result();
		
	}

	public function data_feedback()
	{
		$this->db->select('*');
		$this->db->from('feedback');
		$this->db->order_by('timestamp', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	public function data_pengumuman()
	{
		$this->db->select('*');
		$this->db->from('pengumuman');
		$this->db->order_by('tanggal', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}



}